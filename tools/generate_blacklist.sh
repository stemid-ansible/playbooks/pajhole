#!/usr/bin/env bash

# Get more lists: https://firebog.net/

# List that looks like a hosts file and needs to be split, and skip any
# localhost entry.
declare -a hostsLists=(
  'https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts'
  'http://sysctl.org/cameleon/hosts'
  'https://hosts-file.net/ad_servers.txt'
)

# List with just one host per line, only skips comments and blank lines.
declare -a normalLists=(
  'https://mirror1.malwaredomains.com/files/justdomains'
  'https://s3.amazonaws.com/lists.disconnect.me/simple_tracking.txt'
  'https://s3.amazonaws.com/lists.disconnect.me/simple_ad.txt'
)

# Some lists include hosts you might not want to blacklist, so exclude them
# here.
declare -a whitelistedHosts=(
  'localhost'
  'localhost.localdomain'
  'local'
  'ip6-localhost'
  'ip6-loopback'
  '0.0.0.0'
)

CR=$'\r'

function whitelisted {
  local item="$1"

  for i in ${whitelistedHosts[@]}; do
    [[ "$i" == "$item" ]] && return 0
  done
  #if [[ $list =~ (^|[[:space:]])"$item"($|[[:space:]]) ]]; then
  #  # Yes, item exists in list.
  #  return 0
  #fi

  return 1
}

for list in ${hostsLists[@]}; do
  echo "# List: ${list}"
  counter=0
  while read -ra line; do
    [[ "${line[0]}" == \#* ]] && continue
    [ -z "${line[0]}" ] && continue
    [ -z "${line[1]}" ] && continue
    whitelisted "${line[1]}" && continue

    echo "local-zone: '${line[1]%$CR}' always_nxdomain"
    ((counter++))
  done < <(curl -Ls "$list")
  echo "# EOL: ${list} (${counter} hosts)"
  echo
done

for list in ${normalLists[@]}; do
  echo "# List: ${list}"
  counter=0
  while read -r line; do
    [[ "${line}" == \#* ]] && continue
    [ -z "${line}" ] && continue
    whitelisted "${line}" && continue

    echo "local-zone: '${line%$CR}' always_nxdomain"
    ((counter++))
  done < <(curl -Ls "$list")
  echo "# EOL: ${list} (${counter} hosts)"
  echo
done

