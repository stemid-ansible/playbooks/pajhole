# Pajhole

This is my own version of PiHole. Why?

* I don't want all the web GUI jazz, just the lists.
* I want to use unbound.

# How?

Generate a blacklist with this command:

    $ bash tools/generate_blacklist.sh

Pipe it into a file:

    $ bash tools/generate_blacklist.sh > templates/blacklist.conf

You might want to look at the file to make sure it doesn't look crazy. The shell script is very short and simple, read the comments.

# Ansible install

Install dependency roles.

    $ ansible-galaxy install -r requirements.yml

## Ansible in vagrant

Install unbound with the blacklist in a vagrant VM, to setup your own environment see next headline.

    $ vagrant up
    $ vagrant ssh-config > ~/.ssh/vagrant.conf
    $ ansible-playbook -i inventory/hosts.default site.yml

## Ansible install in your own environment

Make your own inventory and vars files.

    $ cp vars/default.yml vars/myenv.yml
    $ cp inventory/hosts.default inventory/hosts.myenv

Edit ``inventory/hosts.myenv`` and make sure the environment variable matches your environment name.

    [all:vars]
    pajhole_environment=myenv

Also add your own hosts under each host group.

Edit ``vars/myenv.yml`` to match your preferences.

Run for your own environment.

    $ ansible-playbook -i inventory/hosts.myenv site.yml
